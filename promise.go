package promise

import (
	"fmt"
	"os"
	"reflect"
	"sync"

	"github.com/dop251/goja"
)

// State is Pending Fulfilled or Rejected
type State int

// State constants
const (
	Pending State = iota
	Fulfilled
	Rejected
)

// promiseHelper is a goja promise
type promiseHelper struct {
	state       State
	onFulfilled []func(value goja.Value)
	onRejected  []func(reason goja.Value)
	vm          *goja.Runtime
	tasks       chan<- func()
	value       goja.Value
	factory     *Factory
	l           sync.Mutex
}

func bindValue(fn func(value goja.Value), value goja.Value) func() {
	return func() { fn(value) }
}

// Fulfill fulfills the promise p
func (p *promiseHelper) Fulfill(value goja.Value) {
	p.l.Lock()
	defer p.l.Unlock()

	if p.state != Pending {
		return
	}

	p.value = value
	p.state = Fulfilled
	for _, onFulfilled := range p.onFulfilled {
		p.tasks <- bindValue(onFulfilled, value)
	}
}

// Reject rejects the promise p with reason
func (p *promiseHelper) Reject(reason goja.Value) {
	p.l.Lock()
	defer p.l.Unlock()

	if p.state != Pending {
		return
	}

	p.value = reason
	p.state = Rejected

	for _, onRejected := range p.onRejected {
		p.tasks <- bindValue(onRejected, p.value)
	}

	if len(p.onRejected) <= 0 {
		fmt.Fprintln(os.Stderr, "Ignoring error: ", reason)
	}
}

// valueNOP takes one goja.Value and does nothing
func valueNOP(goja.Value) {}

// Calls onFullfiled or onRespected when the promise is fulfilled / rejected
// the callbacks are called in the context of the event loop
func (p *promiseHelper) Then(onFulfilled, onRejected func(goja.Value)) {
	if onFulfilled == nil {
		onFulfilled = valueNOP
	}

	if onRejected == nil {
		onRejected = valueNOP
	}

	p.addHandlers(onFulfilled, onRejected)
}

func (p *promiseHelper) addHandlers(handleFulfilled, handleRejected func(goja.Value)) {
	p.l.Lock()
	defer p.l.Unlock()

	switch p.state {
	case Pending:
		p.onFulfilled = append(p.onFulfilled, handleFulfilled)
		p.onRejected = append(p.onRejected, handleRejected)

	case Fulfilled:
		p.tasks <- func() { handleFulfilled(p.value) }

	default: /* Rejected */
		p.tasks <- func() { handleRejected(p.value) }
	}
}

func (p *promiseHelper) jsThen(call goja.FunctionCall) goja.Value {
	var onFulfilled, onRejected goja.Callable
	resultPromiseInt, resultPromiseValue := p.factory.Create()
	resultPromise := resultPromiseInt.(*promiseHelper)

	p.vm.ExportTo(call.Argument(0), &onFulfilled)
	p.vm.ExportTo(call.Argument(1), &onRejected)

	var handleFulfilled, handleRejected func(goja.Value)

	if onFulfilled == nil {
		handleFulfilled = resultPromise.Fulfill
	} else {
		handleFulfilled = func(value goja.Value) {
			result, err := onFulfilled(goja.Undefined(), value)
			if err != nil {
				resultPromise.Reject(err.(*goja.Exception).Value())
			} else {
				resolve(resultPromise, result)
			}
		}
	}

	if onRejected == nil {
		handleRejected = resultPromise.Reject
	} else {
		handleRejected = func(reason goja.Value) {
			result, err := onRejected(goja.Undefined(), reason)
			if err != nil {
				resultPromise.Reject(err.(*goja.Exception).Value())
			} else {
				resolve(resultPromise, result)
			}
		}
	}

	p.addHandlers(handleFulfilled, handleRejected)

	return resultPromiseValue
}

func (p *promiseHelper) jsCatch(call goja.FunctionCall) goja.Value {
	return p.jsThen(goja.FunctionCall{
		This:      call.This,
		Arguments: []goja.Value{goja.Undefined(), call.Argument(0)},
	})
}

func (p *promiseHelper) jsFinally(call goja.FunctionCall) goja.Value {
	var onFinally goja.Callable
	p.vm.ExportTo(call.Argument(0), &onFinally)

	res, resObj := p.factory.Create()

	var handleFinally func(goja.Value)

	if onFinally == nil {
		handleFinally = func(goja.Value) {
			res.Fulfill(goja.Undefined())
		}
	} else {
		handleFinally = func(goja.Value) {
			r, err := onFinally(goja.Undefined())
			if err != nil {
				res.Reject(err.(*goja.Exception).Value())
			} else {
				resolve(res.(*promiseHelper), r)
			}
		}
	}

	p.addHandlers(handleFinally, handleFinally)
	return resObj
}

func resolve(promise *promiseHelper, x goja.Value) {
	if !goja.IsNull(x) && !goja.IsUndefined(x) {
		xObj := x.ToObject(promise.vm)
		if wrapped := xObj.Get("__wrapped__"); wrapped != nil {
			if x, ok := wrapped.Export().(*promiseHelper); ok {
				if promise == x {
					promise.Reject(promise.vm.NewTypeError("can not resolve to itself"))
				} else {
					if x.state == Pending {
						x.onFulfilled = append(x.onFulfilled, promise.Fulfill)
						x.onRejected = append(x.onRejected, promise.Reject)
					} else if x.state == Fulfilled {
						promise.Fulfill(x.value)
					} else /* x.state == Rejected */ {
						promise.Reject(x.value)
					}
				}
				return
			}
		}

		if then := xObj.Get("then"); then != nil {
			if then, ok := goja.AssertFunction(then); ok {
				// we have a thenable
				fulfillFunc := promise.vm.ToValue(func(call goja.FunctionCall) goja.Value {
					promise.Fulfill(call.Argument(0))
					return goja.Undefined()
				})

				rejectFunc := promise.vm.ToValue(func(call goja.FunctionCall) goja.Value {
					promise.Reject(call.Argument(0))
					return goja.Undefined()
				})

				_, err := then(x, fulfillFunc, rejectFunc)

				if err != nil {
					if promise.state == Pending {
						promise.Reject(err.(*goja.Exception).Value())
					} else {
						fmt.Fprintln(os.Stdout, "ignoring error", err)
					}
				}
				return
			}
		}

	}

	promise.Fulfill(x)
}

// Promise implements the promise interface
type Promise interface {
	Fulfill(value goja.Value)
	Reject(reason goja.Value)
	Then(onFulfilled, onRejected func(goja.Value))
}

// Factory is a promise Factory bound to a goja.Runtime
type Factory struct {
	vm *goja.Runtime
	v  goja.Value
}

// Create creates a new pending promise
func (c *Factory) Create() (Promise, *goja.Object) {
	obj, err := c.vm.New(c.v)
	if err != nil {
		panic(err)
	}

	return obj.Get("__wrapped__").Export().(*promiseHelper), obj
}

func (c *Factory) jsAll(call goja.FunctionCall) goja.Value {
	var promises []goja.Value
	resPromise, resObj := c.Create()
	err := c.vm.ExportTo(call.Argument(0), &promises)
	if err != nil || len(promises) <= 0 {
		resPromise.Fulfill(goja.Undefined())
		return resObj
	}

	results := make([]goja.Value, len(promises))
	resolved := 0

	for i, x := range promises {
		func(i int, x goja.Value) {
			p, _ := c.Create()
			p.Then(func(value goja.Value) {
				// this code is executed by the event loop so no sync lock is required
				results[i] = value
				resolved++
				if resolved == len(results) {
					resPromise.Fulfill(c.vm.ToValue(results))
				}
			}, p.Reject)
			pInst := p.(*promiseHelper)
			pInst.tasks <- bindValue(func(x goja.Value) { resolve(pInst, x) }, x)
		}(i, x)
	}
	return resObj
}

func (c *Factory) jsRace(call goja.FunctionCall) goja.Value {
	var promises []goja.Value

	resPromise, resPromiseObj := c.Create()

	err := c.vm.ExportTo(call.Argument(0), &promises)
	if err != nil || len(promises) == 0 {
		resPromise.Fulfill(goja.Undefined())
		return resPromiseObj
	}

	for _, x := range promises {
		p, _ := c.Create()
		p.Then(resPromise.Fulfill, resPromise.Reject)

		pInst := p.(*promiseHelper)
		pInst.tasks <- bindValue(func(x goja.Value) { resolve(pInst, x) }, x)
	}

	return resPromiseObj
}

func (c *Factory) jsResolve(call goja.FunctionCall) goja.Value {
	p, o := c.Create()

	Resolve(p, call.Argument(0))
	return o
}

func (c *Factory) jsReject(call goja.FunctionCall) goja.Value {
	p, o := c.Create()

	p.Reject(call.Argument(0))
	return o
}

func newFactory(vm *goja.Runtime, tasks chan<- func()) (f *Factory) {
	f = &Factory{vm: vm}
	jsConstructor := func(call goja.ConstructorCall) *goja.Object {
		helper := &promiseHelper{
			state:   Pending,
			tasks:   tasks,
			vm:      vm,
			factory: f,
		}

		var executor goja.Callable
		vm.ExportTo(call.Argument(0), &executor)

		if executor != nil {
			resolveFunc := func(call goja.FunctionCall) goja.Value {
				resolve(helper, call.Argument(0))
				return goja.Undefined()
			}
			rejectFunc := func(call goja.FunctionCall) goja.Value {
				helper.Reject(call.Argument(0))
				return goja.Undefined()
			}

			tasks <- func() {
				_, err := executor(goja.Undefined(), vm.ToValue(resolveFunc), vm.ToValue(rejectFunc))
				if err != nil {
					helper.Reject(err.(*goja.Exception).Value())
				}
			}
		}

		call.This.Set("then", helper.jsThen)
		call.This.Set("catch", helper.jsCatch)
		call.This.Set("finally", helper.jsFinally)
		call.This.Set("__wrapped__", helper)

		return nil
	}
	f.v = vm.ToValue(jsConstructor)

	// define static methods
	obj := f.v.ToObject(vm)
	obj.Set("resolve", f.jsResolve)
	obj.Set("reject", f.jsReject)
	obj.Set("all", f.jsAll)
	obj.Set("race", f.jsRace)

	return
}

// Enable enables promises for the specified runtime
// schedule must enqueue the function and call it in the event loop
// with the specified arguments
func Enable(vm *goja.Runtime, tasks chan<- func()) *Factory {
	c := newFactory(vm, tasks)

	vm.Set("Promise", c.v)
	return c
}

// Resolve implements the promise resolve procedure
func Resolve(promise Promise, x goja.Value) {
	p := promise.(*promiseHelper)

	p.tasks <- func() {
		resolve(p, x)
	}
}

var promiseHelperType = reflect.TypeOf((*promiseHelper)(nil))

// IsPromise returns if o is a promise
func IsPromise(o *goja.Object) bool {
	return o.Get("__wrapped__").ExportType() == promiseHelperType
}
